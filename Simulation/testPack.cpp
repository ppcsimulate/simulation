#include "Globals.h"

#include "package.h"
#include "item.h"

int main(int argc,char *argv[])
{
    Item<int> it; 
    it.add_item(1,1);
    it.print_items();
    //Package<Item<int> > pack; 
    Package<int> pack;
    pack.myprint();
    return 0;
}
