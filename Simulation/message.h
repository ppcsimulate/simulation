#ifndef MESSAGE_H
#define MESSAGE_H

#include "Globals.h"
#include "item.h"
// communication functions using the package and the item headers
template <typename T>
void map_to_item(const map<int,T> &items, Item<T> &I)
{
    // add all the items to the item object
    tr(items,it)
    {
        I.add_item(it->first,it->second);
    }
}

#endif
