MPI Based Rigid Body Simiulation:

*******************************************

Following is brief description of the headers:

rigidmake : is the project make file which produces rigid the executable.


body.h : Defines the Body class which depends on the vector3 class to represent the position and the forces.

boundary.h : Defines a set of functions to detect bodies in a process that cross its domain.

fpsolver.h : Set of functions that finds pairs of bodies that are contact, compute the collision force etc.

generator.h : Generates the spheres

Globals.h : typedefs and Defines used throughout the program

item.h : A template class to hold the message can be either Body or vector3 depending on the type of message

message.h : communication functions using the package and the item headers

package.h : A template class the handles the communication

statistics.h : Collects the statistics.

TimeTracker.h : Collects the run time of various operations.

update.h : Updates the force, velocity and the force of the bodies.

vector3.h : Class used to denote a 3D vector and is based on vector2.h

Contributions : 

*******************************************************

Pranay Anchuri : Worked on the MPI communication and modules that are used before calling the solver. In terms of report, wrote some part of the simulation and results etc.


Ying Lu : Worked on the physics part of the problem. Implemented the solver in c++ and wrote the intial sections and interpretation of the results.

Jielei Wang : Participated in the initial meetings and helped in the function that updates the forces.
