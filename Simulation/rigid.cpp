/*
 *
 *
 * This is the main executable for the rigid body simulation 
 */
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <iostream>
#include "Globals.h"
#include "generator.h"
//#include "communication.h"
#include "message.h"
#include "package.h"
#include "item.h"
#include "boundary.h"
#include "body.h"
#include "vector3.h"
#include "fpsolver.h"
#include "update.h"
#include "statistics.h"
#include "TimeTracker.h"

typedef std::map<int,Body> domain; //  key is the unique id of the ball and the value is the body object

namespace mpi = boost::mpi;

float retain; // number of spheres per processor initially
double radius; // radius of each sphere
int iterations; // number of iterations of the entire simulation
bool print=false;

void print_usage(char *prog) 
{
    std::cerr<<"Usage: "<<prog<<" -np nprocs -F retain -R radius -I iterations -[P] print"<<std::endl;
    exit(0);
}

void parse_args(int argc, char* argv[]) 
{
    if(argc<4) 
    {
        print_usage(argv[0]);
    }

    for (int i=1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-F") == 0)
        {
            retain = atof(argv[++i]);
        }
        else if (strcmp(argv[i], "-R") == 0)
        {
            radius = atof(argv[++i]);
        }
        else if (strcmp(argv[i], "-I") == 0)
        {
            iterations = atoi(argv[++i]);
        }
        else if (strcmp(argv[i], "-P") == 0)
        {
            print = true;
        }
    }
}//end parse_args()

int main(int argc,char *argv[])
{
    // ********************************* Varaibles **************************** //
    int myrank,nprocs;
    domain mydomain; // all the bodies in the current rank's domain..it is mergeed with the bodies received from the neighbors and the solver is called.
    Item<Body> idomain; // an item object to hold the spheres
    Item<Body> msg; // bodies received from the neighbors
    Item<vector3> force; // force of the local bodies
    Item<vector3> vmsg; // vector msg from the neighbors
    int max_number; // max number of spheres that are present in each rank before pruning i.e number if retain = 1
    mii returnids; // the destination of the spheres to send the computed the forces
    mii stat;
    mpi::environment env(argc, argv);
    mpi::communicator world;
    Stat st;
    Tracker trk(2670000000); // stores the times for various operations





    // ******************************* Initialization ************************* //
    parse_args(argc,argv);
    myrank = world.rank();
    nprocs = world.size();
    max_number = maxnumber(radius,nprocs);
    fill_layers(mydomain,radius,myrank,nprocs,myrank*max_number,retain);
    idomain.add_items(mydomain); // all the items inserted into the Item object
    if(myrank==0)
        cout<<"iterations:" << iterations << "\nRadius:" << radius << "\nRetain:" << retain << "\nNumber of spheres:" << idomain.size() << endl;
    Package<Item<Body> > pck(myrank,nprocs); // for sending the bodies
    Package<Item<vector3> > vecpck(myrank,nprocs); // for sending the vector3

    if(myrank==0)
    {
        Body colliding = Body(vector3(0.5,0.95,0.65),1,0.1);
        idomain.item.insert(make_pair(colliding.get_id(),colliding));
    }


    // ***************************** Iterations ******************************** // 
    REP(i,0,iterations)
    {
        // 0 )  append the positions of the bodies
        //if(myrank==0)
        //idomain.print();
        st.append_position(idomain.item);
        // 1 ) send the spheres that cross the domain to the corresponding sphere 

        trk.start("send");
        pck.send(idomain.item,world);
        trk.end("send");

        trk.start("recv");
        pck.recv(world,msg);
        trk.end("recv");
        msg.add_items(idomain.item);
        /*if(myrank==1)
        {
            cout<<" number of spheres received is " <<msg.size()<<endl;
        }*/

        // 2 ) Call the solver
        trk.start("solve");
        solve(msg.item,force.item,returnids);
        trk.end("solve");

        // 3 ) send the forces of the neighbors

        trk.start("send");
        vecpck.vsend(force.item,returnids,world);
        trk.end("send");

        trk.start("recv");
        vecpck.recv(world,vmsg);
        trk.end("recv");
        /*if(myrank==0)
        {
            cout<<" number of vector forces received are "<<vmsg.size()<<endl;
            vmsg.print();
        }*/
        force.merge(vmsg);

        // 4 ) update the velocities

        update_all_velocity(idomain,force);
        manage_bodies(idomain,msg,myrank); // remove the bodies from idomain if it crosses the boundary and add the bodies from the msg to idomain 
        pck.cleanup();
        vecpck.cleanup();
        msg.clear();
        vmsg.clear();
    }
    if(myrank==0)
    {
        st.write_position(myrank);
        trk.print_all();
    }
    return 0;






    /*domain mydomain;
    mii stat;
    map<int,vector3 > force;
//    srand((unsigned)time(NULL));
    fill_layers(mydomain,0.1,0,2,0,0.01);
    Body colliding = Body(vector3(0.5,0.5,0.65),1,0.1);
    mydomain.insert(make_pair(colliding.get_id(),colliding));*/
    //void fill_layers(vector<Body> &mydomain,double r,int rank,int boundary,int startIndex,float retain=0.1)
    /*std::cout<<"the size is "<<mydomain.size()<<std::endl;
    tr(mydomain,it)
    {
        cout<<" the id is "<<it->first<<endl;
        (it->second).print();

    }
    double psi;
    vd mass;
    vd gn;
    vd ext;
    vd vel;
    vector<ii> contacts;
    std::cout<<"psi "<<psi<<std::endl;
    compute_psi(mydomain[1],mydomain[110],psi);
    std::cout<<"mass "<<psi<<std::endl;
    construct_mass(mydomain[1],mydomain[110],mass);
    std::cout<<"gn "<<psi<<std::endl;
    construct_gn(mydomain[1],mydomain[110],gn);
    std::cout<<"ext "<<psi<<std::endl;
    construct_ext(mydomain[1],mydomain[110],ext);
    std::cout<<"vel "<<psi<<std::endl;
    construct_vel(mydomain[1],mydomain[110],vel);
    iterative_step(psi,gn,ext,vel);
    find_contact(mydomain,contacts);
    print_vii(contacts);
    solve(mydomain,force);
    tr(force,it)
    {
        std::cout<<"Total force of "<<it->first<<std::endl;
        it->second.print();
    } 
    // ************************** MPI
    int myrank,nprocs;
    //parse_args(argc,argv);
    mpi::environment env(argc, argv);
    mpi::communicator world;
    myrank = world.rank();
    nprocs = world.size();
    Package<Item<Body> > pck(myrank,nprocs);
    Package<Item<vector3> > vecpck(myrank,nprocs);
    mii returnids; //  the process to which the return forces has to be sent
    if(myrank==0)
    {
        Body colliding = Body(vector3(0.8,0.9,0.4),1,0.1);
        mydomain.insert(make_pair(colliding.get_id(),colliding));
        Body colliding2 = Body(vector3(1.4,0.95,0.5),2,0.1);
        mydomain.insert(make_pair(colliding2.get_id(),colliding2));
        cross_boundary(mydomain,stat,myrank);
        pck.send(mydomain,world);
        // receive the message
        Item<vector3> localforce;
        localforce.add_item(2,vector3(1,1,1));
        Item<vector3> vmsg; // vector msg
        vmsg.add_item(2,vector3(-1,-1,-1));
        vecpck.recv(world,vmsg);
        //cout<<" the number of forces = "<<vmsg.size()<<endl;
        //vmsg.print();
        Item<Body> testBody(mydomain);
        cout<<"before"<<endl;
        testBody.print();
        update_all_velocity(testBody,vmsg);
        cout<<"after"<<endl;
        testBody.print();
        //map_to_item(mydomain,it);
        //std::cout << "I am process " << world.rank() << " of " << world.size()
        //<< "." << mydomain.size()<<std::endl;
        tr(stat,it)
        {
            cout<<it->first<<" and the status is "<<it->second<<endl;
        }
        //pass_information(mydomain,stat,myrank,world);
    }
    else
    {
        Body colliding3 = Body(vector3(1.4,1.03,0.5),3,0.1);
        mydomain.insert(make_pair(colliding3.get_id(),colliding3));
        Body colliding4 = Body(vector3(1.5,1.6,1.4),4,0.1);
        mydomain.insert(make_pair(colliding4.get_id(),colliding4));
        Item<Body> msg(mydomain);
        pck.recv(world,msg);
        //pck.print();
        //msg.print();
        solve(msg.item,force,returnids);
        cout<<" the msg size is"<<msg.size()<<endl;
        vecpck.vsend(force,returnids,world);
        domain neighbors;
        receive_information(world,myrank,neighbors);
        merge_domain(mydomain,neighbors);
        tr(mydomain,it)
        {
            cout<<"the id is "<<it->first<<endl;
            (it->second).print();
        }
        solve(mydomain,force);
        // send back the updated information of the neighbors
        tr(force,it)
        {
            std::cout<<"Total force of "<<it->first<<std::endl;
            it->second.print();
        } 

    }*/
}
