\documentclass[10pt, conference, compsocconf]{IEEEtran}
\IEEEoverridecommandlockouts
%%%% PACKAGES USED%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[tight,footnotesize]{subfigure}
%\usepackage{float}
%\usepackage{verbatim}
%\usepackage{stfloats}
\usepackage{cite}
\usepackage{graphicx}
\graphicspath{{./figures/}}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amsthm,amsfonts,amssymb}
\usepackage{float}
\interdisplaylinepenalty=2500
\usepackage{url}

\usepackage{xspace}
\usepackage{setspace}
\usepackage{tikz}
%\usepackage{caption}
\usepackage{subfigure}
\usepackage{ulem}
\usepackage{array}
\usetikzlibrary{snakes,arrows,shapes}
\usetikzlibrary{positioning}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{pstricks,pst-plot,pst-node,pst-grad,pstricks-add,pst-tree,pst-slpe}
\newcommand{\pw}[1]{\pscirclebox[linewidth=0.4pt,
  fillcolor=cyan!40,fillstyle=solid]{#1}}
  \newcommand\arc[4]{\ncline{#1}{#2}{#3}\ncput{\colorbox{gray!20}{#4}}}
\newcommand\narc[4]{\ncline{#1}{#2}{#3}\ncput{\colorbox{violet!30}{#4}}}

%\usepackage[linesnumbered,boxed,commentsnumbered]{algorithm2e}


%%%%% Theorem definitions and lemma etc%%%%
\newtheorem*{mydef}{Definition}
\newtheorem{thm}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{myclaim}{Claim}

\newcommand{\alg}{CMDB-Miner\xspace}
\newcommand{\vect}[1]{{\boldsymbol #1}}
\newcommand{\bu}{\vect{u}}


%%%%%%%%%%% new for algorithm
\floatname{algorithm}{Procedure}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\let\oldhat\hat
\renewcommand{\vec}[1]{\mathbf{#1}}
\renewcommand{\hat}[1]{\oldhat{\mathbf{#1}}}
\newcommand{\vmprod}[3]{%
%\vec{#1}^\intercal\!\vec{#2}\vec{#3}
   \vec{#1}^\intercal \vec{#2}\mskip\thinmuskip \vec{#3}%
}
% correct bad hyphenation here
%\hyphenation{}

% minimum amount of text per page
\renewcommand{\topfraction}{0.85}
\renewcommand{\textfraction}{0.2}

\begin{document}
\title{MPI Based Rigid Body Simulation}

% author names and affiliations
% use a multiple column layout for up to two different
% affiliations
\author{
\IEEEauthorblockN{Pranay Anchuri, Ying Lu, Jielei Wang,
Christopher Carothers
}

\IEEEauthorblockA{%
\{anchup,luy4,wangj22, carotc\}@rpi.edu\\
Department of Computer Science, 
Rensselaer Polytechnic Institute, Troy, NY 12180, USA\\
}
}



% make the title area
\maketitle


\begin{abstract}
  rigid body dynamics is an area of interest for scientific research, engineering applications, games and computer vision. To accurately simulate the behavior of undeformable rigid bodies draws more attention in related areas. However, to solve the underlying complementarity problem efficiently in real time and to keep high physical accuracy at the same time has been a challenge for decades. Thanks to the massive parallelism available on modern GPUs, we are able to implement the accurate fix-point method on multiprocessors.  In this paper, we solve the complementarity problem by forming a prox function instead of the traditional pivoting method to solve the LCP. Besides, we also present a MPI parallelization and analyze the parallel algorithm as well as the communication between the processors. At last, the scaling behavior of this algorithm is analyzed closely.
\end{abstract}


\begin{IEEEkeywords}
  Rigid body dynamics, MPI, LCP
\end{IEEEkeywords}

\section{Introduction}
The field of rigid body dynamics is a branch of the classical mechanics. The assumption made in this field is that the bodies are undeformable. This assumption is a rigorous idealization of the property of real materials. Rigid body dynamics simulation is an integral and important part of many areas, like the computer games, animation software for digital production, special effect in motion films and animation movies. Rigid body simulation has been used for decades in the robotics field to accurately solve for the friction forces.
\\
The existing solvers used to deal with the collisions in the rigid body simulations belong to two categories: the accurate formulations based on the linear or nonlinear complementarity problems (LCP or NCP). These kind of solvers could predict the normal and frictional force accurately, but at a high complexity of O($NlogN$) or even O($N^2$). The lemke and PATH solver fall into this category. In the past ten years, the prox formulation of the equations of motion has risen as a competitive alternative to the well-known LCP and NCP. Besides, the work has proved the equivalence between the two algorithms.  The non-smooth newton solver which is also based on the prox function is also a good candidate for the parallel implementation. Solvers in the second category are those using simplified models to get an approximate simulation. In view of the difference in their accuracy, the former algorithms are used a lot in the robotics community, while the latter are normally adapted in the computer games and virtual reality environment since the main focus is on the speed of the simulation rather than the accuracy. 
\\
The parallel implementation of the solvers in the rigid body dynamics is becoming more popular in recent two decades since the real-time simulation depends much on the efficiency and speed of the simulation. \cite{iglberger} implemented the FFD solver which enables for the first time rigid body simulations of more than one billion rigid bodies. Bullet has released simulations which run entirely on GPU without needing to communicate with a CPU. Solfec also developed both serial and parallel (MPI) versions of the simulation including the collision detection and physics solutions.
\\
The rest of the paper is organized as follows. Section $2$ presents the related work in the field of rigid body dynamics and the parallel implementations of the most commonly used solvers. Section 3 discusses in the physical aspect of the problem and the fixed point iteration approach. In section 4 we discuss the various steps of the simulation. In section 5 we present the results of our simulation and interpret them. We then conclude with the future work.

\section{Related Work}
\subsection{Penalty method}
One of the common ways to prevent inter-penetration between colliding objects is to allow the penetration and then add a spring force $k \times f(x)$ to separate the two colliding bodies, where $x$ is the penetration depth. This method is simple and efficient but it is not accurate as the bodies are rigid, there should be no penetration ideally. Besides, the penalty-based algorithm will fail to prevent the penetration depth when the value of $k$ is not high enough. Penalty-based simulations are thus cannot handle robust contact , they still are widely used in situations where the time performance is a crucial criterion because its efficiency. 

\subsection{Explicit method}
An alternate way to solve the constraint equations is to decide in advance what positions and velocities should be at next time step, instead of adding some forces as the penalty solver. To enforce a desired state and avoid collision, \textbf{some paper} proposes to directly alter the positions or / and velocities at the end of the time step. 
\\
In a similar way, the Coulomb friction is often modeled explicitly, which is the next state of the objects (take-off, stick, slip) and the sliding directions are determined in advance by considering the configuration state and the relative velocity at the current time step. This approach is simple, but it cannot guarantee the satisfyability of the  Coulomb law at the end of the time step.

\subsection{Implicit method} 
The last solution is to consider that the relative velocities and the directions of friction as well as the normal force at the next time step are unknowns of this problem. In this way, computing the normal contact force and the tangential frictional force implicitly is the only way to strictly enforce the proper conditions at the end of the time step without altering according to the information of the current time step.
\\
 Robust approaches have already been demonstrated for dealing with the contact problem, mostly relying on the linear complementarity problem (LCP).  Besides, most algorithms concentrate on solving the Coulomb Law by discretization of the friction cone into a polyhedron, and then solve the discretized equations. The fix-point iteration method we used by formulating the prox function in this paper models the exact Coulomb friction without any discretization or simplification.  
\subsection{Parallel implementation }
Several physics engines have both serial and parallelized version of the LCP (NCP) solver.   \cite{negrut} implemented their method in the Chrono::Engine simulator, which has solved the largest problem to date, $1000000$ independent spheres in a shaken box. The Bullet engine has solved 110k bodies, running 100\% on GPU using OpenCL. \textbf{Solfec} has also implemented the serial and MPI version of the solver, which uses Gauss-Seidel solver. However, for the Chrono::Engine simulator, the stick-slip friction model is regularized. In this paper, we retain an accurate stick-slip friction model which satisfies the Coulombs law perfectly. 

We will provide an MPI implementation of the algorithm in this paper.  By making use of the BoostMPI, we will provide the performance of the parallel part as well as the scaling analysis in the results. 



\section{ Approach}
\subsection{Time-Stepping Method}
\subsubsection{Formulation of dynamics} 
The dynamics of rigid multi-body systems is based on classical Newtonian mechanics, of which the control equation is: $M\dot{\nu}=\sum{f}$. To solve this in the simulation, we need to discretize the equation by approximating of the derivative of time: $\dot{\nu}=\dfrac{\overline{\nu}^{l+1}-\overline{\nu}^{l}}{h}$. where the superscript $l$ represent the current time step and $l+1$ correspondingly represent the next time step. q is the position vector and $ \nu $ is the velocity vector.
\subsubsection{the complementarity condition} 
The simplest condition of the complementarity is the LCP (linear complementarity problem) can be formulated as : given a matrix $\textit{M} \in {\textbf{R}}^{n \times n}$ and a vector $q \in {\textbf{R}}^{n}$, find two vectors, $z \in \textit{M}^{n}$ and $w(z) \in \textbf{R}^{n}$, that satisfies the conditions:
\begin{gather}
w = \textit{M}z + q \\
0 \leqslant w(z) \perp z \geqslant 0 
\end{gather}
The LCP can be expanded further to the Mixed Complementarity Problem(MCP). The MCP does not require the relationship between $w$ and $z$ to be linear. Based on the MCP, there are a series of constraints equations, in which $ \psi_n $ is the gap function, representing the distance between two bodies, while $ \lambda_n $ is the corresponding normal force between two bodies. Equation (4) represent the contact constraints. Besides the contact normal force, we should also take the frictional force into consideration. When the relative velocity $ \sigma$ between the two bodies is greater than 0, there will be frictional force which is in the tangential direction opposite to the direction of the relative velocity. If the body is sliding, then the friction force is equal to $ \mu \lambda_{n}$. If the body is sticks then the friction force is between $ -\mu \lambda_{n}$ and  $ \mu \lambda_{n}$. The equation (4)-(6) represent the requirements body should satisfy.

\begin{gather}
M \overrightarrow{\dot{\boldsymbol{\nu}}} = G_n(\overrightarrow{\boldsymbol{q}}, t){\boldsymbol{\lambda}}_n + G_f(\overrightarrow{\boldsymbol{q}}, t){\boldsymbol{\lambda}}_f+{\boldsymbol{\lambda}_{ext}} \\
0 \leqslant \overrightarrow{\boldsymbol{\lambda}_n} \perp \overrightarrow{\psi_n} \geqslant 0 \\
0 \leqslant \overrightarrow{\boldsymbol{\lambda}_f} \perp G_{f}^{\textsf{T}} \overrightarrow{\boldsymbol{\nu}}+\overrightarrow{\sigma} \\
0 \leqslant \overrightarrow{\sigma} \perp \overrightarrow{\boldsymbol{\lambda}_n}-E^{\textsf T}\overrightarrow{\boldsymbol{\lambda}_f} \geqslant 0
\end{gather}

In the simulation process, the time step is not continuous and to implement this method we should write the equations aforementioned in a discretized form to represent the constraints. When discretizing this, we use $\dot{\nu}=\dfrac{\overline{\nu}^{l+1}-\overline{\nu}^{l}}{h}$ and substitute this in equation (3), then we get the following discretized equations, in which during the discretization, we use the impulse $P_n$ and $P_f$ to replace the force $\lambda_n$ and $\lambda_f$ by multiplying the time step length on both sides of the Newton-Euler equation:

\begin{gather}
M \overrightarrow{\boldsymbol{\nu}}^{l+1}=M\overrightarrow{\boldsymbol{\nu}}^{l}+G_n\boldsymbol{P}_{n}^{l+1}+G_f\boldsymbol{P}_f^{l+1}+\boldsymbol{\lambda}_{ext}^{l+1} \\
\boldsymbol{q}^{l+1} = \boldsymbol{q}^{l} + h\boldsymbol{\nu}^{l+1} \\
0 \leqslant \boldsymbol{P}_{n}^{l+1} \perp \dfrac{\psi_{n}^{l}}{h} + G_n^{T}\boldsymbol{\nu}^{l+1} \\
0 \leqslant \boldsymbol{P}_{f}^{l+1} \perp G^{T}_{f} \boldsymbol{\nu}^{l+1} + \sigma^{l+1} \geqslant 0 \\
0 \leqslant {\overrightarrow{\sigma}}^{l+1} \perp {\overrightarrow{\boldsymbol{P}_{n}}}^{l+1} - E^{T} \overrightarrow{\boldsymbol{P}_{f}}^{l+1} \geqslant 0
\end{gather}
The above equations can also be rewritten into a matrix form (equation (12)), which has the same format as the LCP
equations in (1) and (2). Rearranging terms in the above equations and use $ \overrightarrow{\rho}_{n}^{l+1}$ to represent the right side of equation (9). Similarly, the right side of equation (10) is referred to as $\overrightarrow{\rho}_{f}^{l+1}$ and the right side of the last equation is referred to as $\overrightarrow{s}^{l+1}$. 

\begin{multline}
%\begin{split}
\begin{bmatrix} 0 \\ \overrightarrow{\rho}_{n}^{l+1} \\ \overrightarrow{\rho}_{f}^{l+1} \\ \overrightarrow{s}^{l+1} \end{bmatrix} = \begin{bmatrix} -\textbf{M} & \textbf{W}_n & \textbf{W}_f & 0 \\ \textbf{W}_{n}^{T} & 0 & 0 & 0 \\ \textbf{W}_{f}^{T} & 0 & 0 & \textbf{E} \\ 0 & U & -\textbf{E}^{T} & 0 \end{bmatrix} \begin{bmatrix} \boldsymbol{\nu}^{l+1} \\ \overrightarrow{\boldsymbol{P}_{n}}^{l+1} \\ \overrightarrow{\boldsymbol{P}_{f}}^{l+1} \\ \overrightarrow{\sigma}^{l+1} \end{bmatrix} \\
 + \begin{bmatrix} \textbf{M}\boldsymbol{\nu}^{l+1} + \overrightarrow{\boldsymbol{\lambda}}_{ext} \\ \frac{\psi_{n}^{l}}{h} \\ 0 \\ 0 \end{bmatrix}
%\end{split}
\end{multline}

\subsection{Fixed-point-iteration}
 From the previous section, we got the matrix form of the time-stepping problem and most solvers are based on the matrix form. Lemke uses a pivoting method to solve both LCP and MCP problem. PATH will also solve problem in a format of the matrix form equation (12). However, the lemke has a high complexity of O(NlogN) or even O($N^2$). In this paper, we use a new method which is a fixed-point iteration based on the accurate constraint equations. 
\subsubsection{formulation of f}
The first step of this method is to form a single equation which represent all the contact and frictional constraint equations in a so-called prox function. Besides, it has been proved that the prox function and the LCP formulation method have equivalence with each other. \\
First, we will define how the prox function work by defining a convex set $C\in \textbf{R}^{n}$ and n is the dimension of this set. If the point x is located in the set C, then the prox function will be equal to x itself. However, if the point x is located out of the set, then it will be mapped onto the nearest point belonged to the convex C. 
For the normal contact constraint, we define the convex set C as the $\textbf{R}^{+}$. Then the constraint function could be written in a prox function form:
\begin{equation} 
\lambda_n - prox_{\textbf{R}^{+}}(\lambda_n - r\psi_n) = 0
\end{equation}
It is worth to mention the parameter r is called the "independent auxiliary parameter" and it has an effect on how quickly the solver converges on a solution. We call this r-value in this paper. and its value ranges from 0 to 1 to find an optimal value guarantee the fast convergence in this paper. The friction constraint can be write into the prox function in the same way. Here the  convex set for the friction will be a different one, which is:
\begin{equation}
C_f(\lambda_n) = {\lambda_f \in \textbf{R} | -\mu\lambda_n \leqslant \lambda_f \leqslant \mu\lambda_n}
\end{equation}
In a similar way, the fraction constraint equation which represent the sticking and sliding state of the contact point is represented in the following prox function:
\begin{equation}
\lambda_{f} - prox_{C_f(C_n)}(\lambda_{f} - r\textbf{W}_f^{T}\overrightarrow{\nu}) = 0
\end{equation}
This form is similar to equation (13) we got for the normal contact force form. Since the friction force is dependent on the relative sliding velocity, the gap distance function is replaced with the sliding velocity. The r-value here is exactly the same as that before. Using the same discretization method we used before, the proximal point functions 
\begin{gather*}
\nu^{l+1} = \nu^{l}+M^{-1}G_nP_n^{l+1} + M^{-1}G_fP_f^{l+1} + M^{-1}P_{ext}^{l+1} \\
P_{n}^{l+1} = prox_{\textbf{R}^{+}}(P_{n}^{l+1} - r(\dfrac{\overrightarrow{\psi}_{n}^{l}}{h} + W_{n}^{T}\overrightarrow{\nu}^{l+1})) \\
P_{f}^{l+1} = prox_{C_f(P_n^{l+1})}(P_f^{l+1}-rW_f^{T}\nu^{l+1})
\end{gather*}
\subsubsection{solve the fix-point iteration}
 The proximal point equations are solved through a method called fix point iteration. The basic format of the equation to use this method is as the following: 
 \begin{equation}
 x_{k+1} = f(x_k), k = 0, 1, 2\cdots
 \end{equation}

 There are some situations which cause the system to diverge from a solution instead of converging which is why the choice of the r-value is very important in this problem. In our code , we have tested different values of $r$ in a serial version code to get a value that will guarantee a solution fast and then we chose $r=0.5$ in this paper.

\section{Simulation} 

Figure \ref{fig:process} shows the important steps in the simulation, of which the first step is the body generation, this will be covered in detail in the next sections. The next step is the cross boundary detection, which is to get the information about which spheres that should be sent to the neighbors of a process. In the next step calls to MPI\_Send and MPI\_Recv will communicate the required information between the processes. After the communication, the collision detection and the solver part is executed in local domain of the processor. Once we got the force from the neighbors, another MPI\_Send and MPI\_Recv operation will be called to send back the collision normal force to its local. The last step is to update the velocity and the position of the spheres for the next time step. We describe each of the steps in detail below.

\subsection{Body Generation}
\begin{figure}
\centering
\tikzstyle{decision} = [diamond, draw, fill=blue!20, 
    text width=4.5em, text badly centered, node distance=3cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=blue!20, 
    text width=7em, text centered, rounded corners, minimum height=3.2em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse,fill=red!20, node distance=3cm,
    minimum height=2em]
    \centering
\scalebox{1}[0.65]
{
\begin{tikzpicture}[node distance = 2cm, auto]
    % Place nodes
    \node [block] (init) {Body Generation};
    \node [block, below of=init] (identify) {Cross Boundary Detection};
    \node [block, below of=identify] (evaluate) {MPI\_Send/Recv local/remote info to/from neighbors};
    \node [block, left of=evaluate, node distance=3cm] (update) {go to main loop};
    \node [block, below of= evaluate] (cd) {Collision Detection};
    \node [block, below of= cd] (solver){Call the LCP solver}; 
    \node [block, below of= solver] (update2) {Update Velocity and Position};
  % \node [decision, below of= update2] (deci) {is current step the maximum};
 %   \node [block, below of=deci, node distance=3cm] (stop) {stop};
    % Draw edges
    \path [line] (init) -- (identify);
    \path [line] (identify) -- (evaluate);
   \path [line] (evaluate) -- (cd);
    \path [line] (cd) -- (solver);
    \path [line] (solver) -- (update2);
%     \path [line] (solver) -- (decide);
    \path [line] (update2) -| node [near start] {} (update);
    \path [line] (update) |- node [near start] {} (identify);
%    \path [line] (decide) -- node {no}(stop);    
\end{tikzpicture}
}
\caption{The simulation procedure for one time step}
\label{fig:process}
\end{figure}
 
\begin{figure}[!hbt]
\centering
	
	\begin{pspicture}(0,0)(3,3)
	\psaxes[ticks=y,Dy=1,Oy=0](0,0)(0,0)(3,3)
	%\psgrid
	\pscircle[linewidth=0.5pt](0.25,0.25){0.25}
	\pscircle[linewidth=0.5pt](0.25,0.75){0.25}
	\pscircle[linewidth=0.5pt](0.75,0.25){0.25}
	\pscircle[linewidth=0.5pt](0.75,0.75){0.25}
	\pscircle[linewidth=0.5pt](1.25,0.25){0.25}
	\pscircle[linewidth=0.5pt](1.25,0.75){0.25} 
	\pscircle[linewidth=0.5pt](1.75,0.25){0.25}
	\pscircle[linewidth=0.5pt](1.75,0.75){0.25}
	\pscircle[linewidth=0.5pt](2.25,0.25){0.25}
	\pscircle[linewidth=0.5pt](2.25,0.75){0.25}
	\pscircle[linewidth=0.5pt](2.75,0.25){0.25}
	\pscircle[linewidth=0.5pt](2.75,0.75){0.25}
	
	% layer 2
	
	\pscircle[linewidth=0.5pt](0.25,1.25){0.25}
	\pscircle[linewidth=0.5pt](0.25,1.75){0.25}
	\pscircle[linewidth=0.5pt](0.75,1.25){0.25}
	\pscircle[linewidth=0.5pt](0.75,1.75){0.25}
	\pscircle[linewidth=0.5pt](1.25,1.25){0.25}
	\pscircle[linewidth=0.5pt](1.25,1.75){0.25}
	\pscircle[linewidth=0.5pt](1.75,1.25){0.25}
	\pscircle[linewidth=0.5pt](1.75,1.75){0.25}
	\pscircle[linewidth=0.5pt](2.25,1.25){0.25}
	\pscircle[linewidth=0.5pt](2.25,1.75){0.25}
	\pscircle[linewidth=0.5pt](2.75,1.25){0.25}
	\pscircle[linewidth=0.5pt](2.75,1.75){0.25}
 
	% layer 3
	
	\pscircle[linewidth=0.5pt](0.25,2.25){0.25}
	\pscircle[linewidth=0.5pt](0.25,2.75){0.25}
	\pscircle[linewidth=0.5pt](0.75,2.25){0.25}
	\pscircle[linewidth=0.5pt](0.75,2.75){0.25}
	
	\pscircle[linewidth=0.5pt](1.25,2.25){0.25}
	\pscircle[linewidth=0.5pt](1.25,2.75){0.25}
	\pscircle[linewidth=0.5pt](1.75,2.25){0.25}
	\pscircle[linewidth=0.5pt](1.75,2.75){0.25}
	
	\pscircle[linewidth=0.5pt](2.25,2.25){0.25}
	\pscircle[linewidth=0.5pt](2.25,2.75){0.25}
	\pscircle[linewidth=0.5pt](2.75,2.25){0.25}
	\pscircle[linewidth=0.5pt](2.75,2.75){0.25}
	\end{pspicture}
	
	\caption{Shows the initial state of the cube with $p=3$ and the radius of each sphere is $0.25$.}
\label{fig:layers}
\end{figure}

\begin{figure}[!hbt]
\centering

	
	\begin{pspicture}(0,0)(3,3)
	\psaxes[ticks=y,Dy=1,Oy=0](0,0)(0,0)(3,3)
	%\psgrid
	
	\pscircle[linewidth=1pt](0.75,0.75){0.25}
	\pscircle[linewidth=1pt](1.75,0.75){0.25}
	\pscircle[linewidth=1pt](2.25,0.25){0.25}
	% layer 2
	
	\pscircle[linewidth=1pt](0.75,1.25){0.25}	
	\pscircle[linewidth=1pt](1.75,1.75){0.25}
	\pscircle[linewidth=1pt](2.25,1.25){0.25}
	
	% layer 3

	\pscircle[linewidth=1pt](0.25,2.75){0.25}		
	\pscircle[linewidth=1pt](1.25,2.25){0.25}
	\pscircle[linewidth=1pt](2.75,2.75){0.25}
	\end{pspicture}
	
\caption{Spheres reamining in each rank after retaining only a fraction $r=0.25$ of the initial spheres.}
\label{fig:retain}
\end{figure}



The first step in simulating the rigid body dynamics is generating the spheres such that the following conditions are hold.

\begin{itemize}
	\item Each sphere has a globally unique $id$.
	\item The spheres donot penetrate when generated initially.
	\item The initial positions of the spheres are random.
\end{itemize}
To make things uniform and easier, we restricted the simulation universe to a $(p,p,p)$ cube  where $p$ is the number of ranks. The domain of a rank $i$ is strip of the cube with $Y$ axis restricted to $[i,i+1)$. Each rank expect $0$ and $p-1$ has two neighbors. Given the radius of the sphere, we completely fill the cube with the spheres giving each a unique id. If $M$ is the maximum number of spheres that can fit each rank without penetrating, then the $id$ of a sphere in the rank $i \in [i \times M, (i+1) \times M)$. This makes sure that the $id$ is unique globally without communicating with other ranks in the system. Given the retaining factor $r$, we randomly remove $(1-r)$ fraction of spheres from each rank. This makes sure that spheres can move freely and the simulation can proceed. 


Figure \ref{fig:layers} shows a $2$D cross section of the initial state when the cube is completely filled with spheres. The number of ranks $p=3$. Figure \ref{fig:retain} shows the spheres remaining in each rank when retaining factor $r=0.25$.

\subsection{Cross Boundary Detection}
In each iteration of the simulation, we first find the spheres that cross the domain of a process. As the domain of each process is a slice of the cube with a unit length it is easy to find bodies that cross the domain using a single pass over all the bodies. A body $B$ crosses boundary depending on the $y$ position of its center and the radius. We denote with $+1$ if it crosses from upper end and $-1$ if it crosses from the lower end.

\begin{equation}
CrossBoundary(B) = \left\{\begin{matrix}
1 & B.y + rad > rank+1 \\ 
-1 & B.y -rad < rank \\ 
 0 & otherwise 
\end{matrix}\right.
\end{equation}

Using a pair of MPI\_SEND and MPI\_RECV, every process sends the bodies that cross its domain and also receives remote bodies from its neighbors. In the next step it computes all contacting bodies and updates the forces.
    
\begin{figure}
\centering
\begin{tikzpicture}[scale = 0.8]
% \hspace{1cm}
 \draw(0,0) rectangle(6,3);
 \draw(0,3)[shade] rectangle(6,6);
 \shade[shading=ball, ball color=red](1.8,3.1) circle(0.5cm);
 \node[right] at (1.55,3.1) {$4$};
 \shade[shading=ball, ball color=red](4.5,2.79) circle(0.5cm);
 \node[right] at (4.25,2.79) {$5$};
 
 \shade[shading=ball, ball color=green](1,3.84) circle(0.5cm);
 \node[right] at (0.75,3.84) {$2$};
 \shade[shading=ball, ball color=green](2.6,5.4) circle(0.5cm);
 \node[right] at (2.35,5.4) {$1$};
 \shade[shading=ball, ball color=green](4.8,3.9) circle(0.5cm);
 \node[right] at (4.55,3.9) {$3$};
 \shade[shading=ball, ball color=green](0.6,1.1) circle(0.5cm);
 \node[right] at (0.35,1.1) {$9$};
 \shade[shading=ball, ball color=green](1.05,2.3) circle(0.5cm);
 \node [right] at (0.8, 2.3) {$6$};
 \shade[shading=ball, ball color=green](2.31,2.11) circle(0.5cm);
 \node [right] at (2.06,2.11) {$7$};
 \shade[shading=ball, ball color=green](4.4,1.6) circle(0.5cm);
 \node [right] at(4.15, 1.6) {$8$};
 
 \draw[style=dashed, line width=1pt](1.6, 2.95) circle(1.64cm);
 
 % \node[align=center, below]
 
\end{tikzpicture}
\caption{Shows nine solid spheres belonging to two ranks. Sphere is local to the processor in which its center lies. $1,2,3,4$ belong to the rank above and $5,6,7,8,9$ belongs to the rank below. The figure also shows the normal force, external force on sphere $4$ due to collision with the other spheres and the gravity respectively.}
\label{fig:distribute}
\end{figure}

\begin{figure}
\centering
\vspace{1cm}
\begin{tikzpicture}[scale = 1.1]
%\hspace{2cm}
  
 \shade[shading=ball, ball color=red](1.8,3.1) circle(0.5cm);
  \node[right] at (1.85,3.1) {$4$};
 \shade[shading=ball, ball color=green](1,3.84) circle(0.5cm);
 \node[right] at (0.65,3.84) {$2$};
 \shade[shading=ball, ball color=green](1.05,2.3) circle(0.5cm);
 \node [right] at (0.75, 2.3) {$6$};
 \shade[shading=ball, ball color=green](2.31,2.11) circle(0.5cm);
 \node [right] at (2.16,2.01) {$7$};
 \draw[style=dashed, line width=1pt](1.6, 2.95) circle(1.64cm);
 \draw[style=dashed,blue, line width=2pt] (1,3.84) -- (1.8,3.1);
 \draw[line width=2pt,blue,->] (1.8,3.1) -- (3,1.99);
 
 \draw[style=dashed,orange, line width=2pt] (1.05,2.3) -- (1.8,3.1);
 \draw[line width=2pt,orange,->] (1.8,3.1) -- (3,4.38);
 
 \draw[style=dashed,orange, line width=2pt] (2.31,2.11) -- (1.8,3.1);
 \draw[line width=2pt,orange,->] (1.8,3.1) -- (0.6,5.429);
 
\end{tikzpicture}
\caption{Shows the communication between the two ranks. The two orange forces are induced by collision with sphere $6,7$ in remote neighbors and they are sent back to the local rank to add to the blue force to  get an total collision contact normal force.}
\label{fig:communication}
\end{figure}
Figure \ref{fig:distribute} shows how we define the local and remote bodies, if a sphere is totally in the domain of a certain processor, we assume it is local to the process, like body $1,2,3$ are totally local to above rank while $6,7,8,9$ are totally local to the down rank; For the green bodies $4,5$. Since they are crossing the boundary of the processor, take sphere 5 as an example, since its center is inside the down rank, it is local to the down rank and remote to the above rank, before calling the solver, we will send the cross boundary sphere to its remote neighbors.

Figure \ref{fig:communication} explains how we send back the information of the spheres from the remote neighbor to its local rank after we call the solver and update the collision normal force. In this figure, the orange forces come from the collision of sphere $4$ with the spheres in the domain of the remote rank, which is $6,7$. Then we add the collision force induced by collision with its local body and the remote ones, the total force can be calculated by adding them up.
 
\subsection{Collision Detection and update velocity}

    Collision detection is the most important step where the collision force are computed for each pair of the colliding bodies. Two bodies are said to be colliding if the distance between them is less than the sum of their radii for each pair. After the collision detection, we will call the solver to solve the bodies that collide and compute for the normal force induced by the collision. With the normal force induced, we update the velocity and the position of the bodies for the next time step. The process continues until required number of iterations.


\subsection{Algorithm}
\begin{algorithm}   [ht]                   % enter the algorithm environment
 \caption{Simulate Rigid Bodies}        % give the algorithm a caption
\label{alg:simulate}                           % and a label for \ref{} commands later in the document
\begin{algorithmic}                    % enter the algorithmic environment

\REQUIRE nprocs = $p$, radius = $r$, retain = $f$, rank = $k$ and iterations = $i$.
\ENSURE Simulate the effect of solid non-penetrating spheres
falling under external force.

\STATE $mydomain \gets GenSpheres(p,r,f,k) $

\FOR{$itr = 1 \to i$}
\STATE $boundary \gets cross(mydomain,k)$ 
\STATE $pass\_info(boundary)$ 
\STATE $Lforce \gets solve(mydomain)$ 
\STATE $pass\_remote(Lforce) $
\STATE $TForce \gets Synchronize(Lforce) $
\STATE $Update\_Pos(TForce)$
\ENDFOR

\end{algorithmic}
\end{algorithm}

\section{Results}
In this section we evaluate the effectiveness of the parallel algorithm in simulating rigid body dynamics. All experiments were performed on a 2.67GHz Intel i7 processor with $4$GB of memory running Ubuntu Linux version 10.04. We have used C++ bindings of Boost.MPI which supports majority of the functionality in MPI 1.1. Entire code is written in C++ and is hosted on $http://bitbucket.org$.
\subsection{Software}
Each sphere is generated by instantiating the class $Body$ which contains the information about its $id$, radius, position, current velocity and the external force. In each iteration the information about the local bodies which cross the boundary needs to be passed to the neighbors where the spheres are local. Since $MPI$ doesn't have native support for sending and receiving user defined data types we have used $Boost MPI$ libraries which makes sending user defined data types easy. Behind the scenes it uses the powerful $Boost Serialization$ libraries to encode the data into a byte stream so that the data can be sent easily. At the receiver end, the data can be decoded back to the original structure by unpacking the byte stream. In this way, the overhead of defining custom MPI data types is avoided. Boost MPI follows skeleton and content technique to send information as structure and content seperately. As the structure of the datatypes remain same over all the iterations, Boost libraries can make use of the this fact and only send the content and use the structure communicated previously to compute full fledged data structure. 
\subsection{verification of the method}
\begin{figure}[ht]
\centering

\subfigure[lemke method]{
   \includegraphics[scale =0.25] {fig/lemke_normal.eps}
   \label{fig:ffig1}
 }
 \subfigure[fix-point iteration]{
   \includegraphics[scale =0.25] {fig/fixpoint_normal.eps}
   \label{fig:ffig2}
 }
\caption{Comparison of contact normal force by the lemke method and the fix-point method}
\label{fig:ffig}
\end{figure}

\begin{figure}[ht]
\centering

\subfigure[lemke method]{
   \includegraphics[scale =0.25] {fig/lemke_velocity.eps}
   \label{fig:vfig1}
 }
 \subfigure[fix-point iteration]{
   \includegraphics[scale =0.25] {fig/fixpoint_velocity.eps}
   \label{fig:vfig2}
 }
\caption{Comparison of velocity by the lemke method and the fix-point method}
\label{fig:vfig}
\end{figure}

To prove that we implement the prox function formulation method correctly, we compare our results of the fix-point iteration method with the well-known lemke method. This experiment is done in MATLAB and there are only three spheres falling down on a three sided pyramidal depression/bowl. We compared the normal contact force and the velocity about sphere $2$ to the lemke method. Figure $6$ shows the result of the lemke method and prox function method. From the result we could see clearly that our method matches well with the lemke method except at the time step of about $66$, sphere $2$ has collision with sphere $3$ but there is no this collision in the fix-point iteration method. This is due to the difference in the frictional force. Since they have this difference in the normal contact force, there is a difference in the velocity correspondingly, which is almost at the same iteration step. Figure $7$ clearly demonstrate this.


\subsection{scaling analysis of results from MPI}

Next we implement this method in MPI, which to be accurately is BoostMPI. Since in this way, we could send the body information as a class and the contact normal force as a vector of size 3 without converting all these to the MPI\_Byte. These can be clearly seen from the source code. By doing in this way, our code becomes more efficient because we don't have to waste time on converting the class of body to MPI\_Byte and correspondingly the neighbors will receive the message in a format of class of body. 

In our experiment, the number of processors range from 2, 4, 8, 16, 32 and 64. From the strong scaling results which shown in Figure $8,9,10$, we could see clearly the linear increase in the time with the increase of the number of processors. Since we generate the spheres initially by layers and then just randomly delete some of them, it is expected that the number of spheres cross the boundary of two neighbor ranks should be almost the same. So the result reflect what we have expected. The receiving time works in a similar way. For the solver time change with the increase of the number of processors, it is less than what we expected. This may due to the fact that the number of spheres in collision with each other is not very large compared with the total number of bodies. Next we will work on the exact pair of bodies which we got from the step of the collision detection in our simulation before.  
\begin{figure}
\centering
   \includegraphics[scale=0.3]{fig/send.eps}
   \label{fig:send}
   \caption{The time spent on sending of the strong scaling experiment of 100 time steps with about 4500 spheres in each rank initially }   
\end{figure}
 
\begin{figure}
\centering
   \includegraphics[scale=0.3]{fig/recv.eps}
   \label{fig:recv}
   \caption{The time spent on receiving of the strong scaling experiment of 100 time steps with about 4500 spheres in each rank initially }   
\end{figure}

\begin{figure}
\centering
   \includegraphics[scale=0.3]{fig/solve.eps}
   \label{fig:solve}
   \caption{The time spent on solving the mixed LCP in the strong scaling experiment of 100 time steps with about 4500 spheres in each rank initially }   
\end{figure}

\section{Future Work}
In terms of future work, we would like to compare the performance of $MPI$ and $GPU$ in simulating the rigid body dynamics. In this work, the domain of each rank is a rectangular cuboid which reduces the inter process communication. However, this results in an increases workload on each process. Other variations include hexagonal domain which is a balance between communication and work. We would like to study the effect of domain shape on the performance of the simulation. When it comes to the physics solver part, we would like to work on the frictional force part to ensure the accuracy of the simulation results.
\section{Conclusion}
In this simulation, we used the fix-point iteration method by formulation of the prox function to solve the rigid multi-body dynamics simulation and then we implemented this on MPI. There is a good match of the simulation result with the prox function method and the well-known lemke method. We use strong scaling in the MPI part to keep the number of spheres almost the same in each rank. We got a linear increase of the time spent on sending and receiving as well as the time spent on the solver, which is what we expected. The number of bodies in each rank is about $4000$, but the spheres in collision with others are not so many, that's why the time spent on the solver is relative small compared with the large total number of spheres. So further work will be on the exact number of the spheres that in collision in each rank. 

%\setstretch{0.95}
%\bibliographystyle{plain}
%\bibliography{ref}
\begin{thebibliography}{1}

\bibitem{bender}
Jan Bender, Kenny Erleben, Jeff Trinkle, and Erwin Coumans.
\newblock Interactive simulation of rigid body dynamics in computer graphics.
\newblock In {\em EUROGRAPHICS 2012 State of the Art Reports}. Eurographics
Association, 2012.

\bibitem{bertails}
Florence Bertails-Descoubes, Florent Cadoux, Gilles Daviet, and Vincent Acary.
\newblock A nonsmooth newton solver for capturing exact coulomb friction in
fiber assemblies.
\newblock {\em ACM Trans. Graph.}, 30(1):6:1--6:14, February 2011.

									     \bibitem{iglberger}
										 Klaus Iglberger and Ulrich Rüde.
										 \newblock Massively parallel rigid body dynamics simulations.
										 \newblock {\em Computer Science - Research and Development}, 23:159--167, 2009.

										 \bibitem{ffd}
										 Danny~M. Kaufman, Timothy Edmunds, and Dinesh~K. Pai.
										 \newblock Fast frictional dynamics for rigid bodies.
										 \newblock {\em In ACM SIGGRAPH}, 24:946--956, 2005.

										 \bibitem{lien}
										 Jyh ming Lien, Shawna~L. Thomas, and Nancy~M. Amato.
										 \newblock A general framework for sampling on the medial axis of the free
										 space.
										 \newblock In {\em Proc. IEEE Int. Conf. on Robotics and Automation}, pages
										 4439--4444, 2003.

										 \bibitem{negrut}
										 Dan Negrut, Laurent Jay, Alessandro Tasora, Mihai Anitescu, Hammad Mazhar, Toby
										 Heyn, and Arman Pazouki.
										 \newblock Simulation of multibody dynamics leveraging new numerical methods and 
										 multiprocessor capabilities.

										 \bibitem{binh}
										 Thorsten Schindler, Binh Nguyen, and Jeff Trinkle.
										 \newblock Understanding the difference between prox and complementarity
										 formulations for simulation of systems with contact.
										 \newblock In {\em IROS}, pages 1433--1438. IEEE, 2011.

										 \end{thebibliography}

\end{document}
