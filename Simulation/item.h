#ifndef ITEM_H
#define ITEM_H

#include "Globals.h"
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include "vector3.h"

template <typename ItmTyp>
class Item
{
    private:
        friend class boost::serialization::access;
        template<class Archive>
            void serialize(Archive & ar, const unsigned int version)
            {
                ar & item;
            }
    public:
        inline void add_item(const int id, const ItmTyp &it)
        {
            item.insert(make_pair(id,it));
        }
        inline void clear()
        {
            item.clear();
        }
        inline void add_dummy()
        {
            item.insert(make_pair(-1,ItmTyp()));
        }
        inline void remove_dummy()
        {
            item.erase(-1);
        }
        void merge(const Item &it)
        {
            tr(it.item,x)
            {
                add_item(x->first,x->second);
            }
        }
        int size(){ return item.size();}
        void print()
        {
            cout<<" the items are "<<endl;
            tr(item,it)
            {
                cout<<it->first<<":"<<endl;
                (it->second).print();
            }
        }
        Item(const map<int,ItmTyp> &init)
        {
            item.insert(all(init));
        }
        void add_items(const map<int,ItmTyp> &it)
        {
            item.insert(all(it));
        }
        Item(){}
        map<int,ItmTyp> item;
};

template<>
void Item<vector3>::merge(const Item<vector3> &it)
{
    tr(it.item,I)
    {
        if( present(item,I->first)) // add the forces
        {
            item[I->first] += I->second;
        }
        else
        {
            item[I->first] = I->second;
        }
    }
}
#endif

