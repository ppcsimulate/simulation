#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <iostream>
#include "Globals.h"
#include "body.h"
#include "boundary.h"
namespace mpi = boost::mpi;

typedef std::map<int,Body> domain; //  all the balls in the current processor

void initialize(domain &mydomain)
{
    // insert a dummy body..
    mydomain.insert(make_pair(-1,Body()));
}

void remove_dummy(domain &mydomain)
{
    // remove the dummy messafe with the id -1
    mydomain.erase(-1);
}
void pass_information(domain &mydomain,const mii &stat, int myrank, boost::mpi::communicator &world)
{
    // depending on the status send the objects to neighbors
    int status;
    domain lower;
    domain upper;
    initialize(lower); initialize(upper);
    int nprocs=world.size();
    tr(stat,it)
    {
        status = it->second;
        if(status<0)
            lower.insert(make_pair(it->first,mydomain[it->first]));
        else if(status > 0)
            upper.insert(make_pair(it->first,mydomain[it->first]));
    }
    if(0<=myrank+1 && myrank+1 < nprocs)
        world.send(myrank+1,0,upper);
    if(0<=myrank-1 && myrank-1 < nprocs)
    world.send(myrank-1,0,lower);

}

void receive_information(boost::mpi::communicator &world,int myrank,domain &neighbors)
{
    domain recv_from_upper;
    domain recv_from_lower;
    mpi::request req[2];
    int count=0;
    int nprocs = world.size();
    if(0<=myrank+1 && myrank+1 < nprocs)
    {
        req[count] = world.irecv(myrank+1,0,recv_from_upper);
        count++;
    }
    if(0<=myrank-1 && myrank-1 < nprocs)
    {
        req[count] = world.irecv(myrank-1,0,recv_from_lower);
        count++;
    }
    mpi::wait_all(req,req+count);
    // remove the dummy elements 
    remove_dummy(recv_from_lower);
    remove_dummy(recv_from_upper);

    merge_domain(neighbors,recv_from_lower);
    merge_domain(neighbors,recv_from_upper);
    //neighbors.insert(make_pair(b.get_id(),b));
}

#endif
