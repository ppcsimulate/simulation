#ifndef SOLVER_H
#define SOLVER_H

#include "Globals.h"
#include "body.h"
typedef std::vector<Body> domain; //  all the balls in the current processor
typedef std::vector<vector3> vv3;


bool colliding(const Body &b1, const Body &b2)
{
    // return true if the two bodies are colliding
    double dist = 2 * b1.get_rad(); // is valid only when both the bodies have eqaul radius
    return abs(b1.get_x()-b2.get_x()) <= dist ||  abs(b1.get_y()-b2.get_y()) <= dist ||  abs(b1.get_z()-b2.get_z()) <= dist;
}

bool xcomparator(const Body &b1, const Body &b2)
{
    return b1.get_x() < b2.get_x();
}
void find_contact(domain &bodies,vector<pair<int,int> > &allcontacts)
{
	int i, j, contactid=0;
	vector3 pos1, pos2;
	float dist;
    int sz = bodies.size();
    tr(bodies,it)
	{
        tr(bodies,it2)
		{
            pos1 = it->get_position();
            pos2 = it2->get_position();
            dist = norml(pos1 - pos2);
			if(dist - radius*2 < 1E-6)
			{
			//	psi.push_back(dist - radius*2);
                allcontacts.push_back(ii(it->get_id(),it->get_id()));
			}

		}
	}
}

void compute_psi(const Body &b1, const Body &b2, double &psi)
{
	vector3 pos1, pos2;
	dist = norml(pos1-pos2);
	psi = dist;
}
void construct_mass(const Body &b1,const Body &b2,vd &mass)
{
    double m1,i2,m2,i2;
    m1 = b1.get_mass();
    m2 = b2.get_mass();
    i1 = b1.get_inertia();
    i2 = b1.get_inertia();
    REP(i,0,3) { mass.push_back(m1);   }
    REP(i,0,3) { mass.push_back(i1);   }
    REP(i,0,3) { mass.push_back(m2);   }
    REP(i,0,3) { mass.push_back(i2);   }
}
void construct_gn(const Body &b1,const Body &b2,vd &gn)
{
	vector3 gn1, gn2, gn1_norm, gn2_norm;
	gn1 = b1.get_position() - b2.get_position();
	gn1_norm = gn1.Normalize(gn1);
	gn2_norm = - gn1_norm;
	int i;
	REP(i,0,3){gn.push_back(gn1_norm[i]);}
	REP(i,0,3){gn.push_back(0);          }
	REP(i,0,3){gn.push_back(gn2_norm[i]);}
	REP(i,0,3){gn.push_back(0);          }
}


void construct_ext(const Body &b1,const Body &b2,vd &ext)
{
	vector3 ext1, ext2;
	ext1 = b1.get_ext();
	ext2 = b2.get_ext();
    int i;	
	REP(i,0,3) {ext.push_back(ext1[i]);     }
	REP(i,0,3) {ext.push_back(0);          }
	REP(i,0,3) {ext.push_back(ext2[i]);    }
	REP(i,0,3) {ext.push_back(0);          }
}


void construct_vel(const Body &b1,const Body &b2,vd &vel)
{
	vector3 vel1, vel2;
	vel1 = b1.get_velocity(); 
	vel2 = b2.get_velocity();
	
	REP(i,0,3){vel.push_back(vel1[i]);    }
	REP(i,0,3){vel.push_back(0);          }
	REP(i,0,3){vel.push_back(vel2[i]);    }
	REP(i,0,3){vel.push_back(0);          }
}

void construct_velPlusOne(const vd &mass,const vd&ext,const vd &vel, double &pnPlusOne, const vd &gn, vd &velPlusOne, const int &contactid, const vd &psi)
{
   /*
	*velPlusOne = vel + gn/m * pnPlusOne + gf/mass*pfPlusOne + ext/mass;
	*/
   
   double pn_star, r=0.5, rho_n;
   vd gn_mass;
   vd ext_mass;
   gn_mass = 1/mass * gn * pnPlusOne;  // [12] * [1]
   ext_mass = 1/mass * ext;
   velPlusOne = vel + gn_mass + ext_mass;

   rho_n = psi[contactid] / timestep + gn*velPlusOne;
   pn_star = pnPlusOne - r * rho_n;
   if (pn_star < 0)
   {
	   pn_star = 0;
   }

}

double inner_factor(const vd &gn, const vd &ext, const vd &vel,double pPlusOne)
{
    double ret=0;
    REP(i,0,gn.size())
    {
        ret += gn[i] * ( vel[i] + pPlusOne* gn[i] + ext[i]);
    }
    return ret;
}

bool converged(const double &a, const double &b,double ep=1E-03)
{
    double diff = a-b;
    diff = diff>0 ? diff : -1*diff;
    return diff < ep;
}
double iterative_step(double psi,const vd &gn,const vd &ext,const vd & vel,vd &velPlusOne)
{
    // initialize pnPlusOne
    // TODO : pnPlusOne is a double not a vector of doubles
    double prev=-1; // this is the value that will be returned when the iteration converges
    double temp;
    while(true)
    {
        temp = prev;
        temp += (-0.5) * ( psi + inner_factor(gn,ext,vel,prev));
        if(temp<0)
            temp=0;
        if(converged(temp,prev))
            return temp;
        prev = temp;
    }
}

vector3 pair_solver(Body &b1,Body &b2)
{
    /* Update the information for these bodies
     * */
    // Step 1 : Construct the three vectors corresponding to Mass, External Force and the velocity
    vd mass,ext,vel,gn,velPlusOne;
    double psi,pnPlusOne;



    construct_mass(b1,b2,mass);
    construct_ext(b1,b2,ext);
    construct_vel(b1,b2,vel);
    construct_gn(b1,b2,gn);
    construct_velPlusOne(mass,ext,vel,gn,velPlusOne);
    psi = compute_psi(b1,b2);
    iterative_step(psi,gn,velPlusOne,pnPlusOne);
    return pnPlusOne*vector3(gn[0],gn[1],gn[2]);
}
void total_force(const map<int,vv3> &table,map<int,vector3> &force)
{
    tr(table,it)
    {
        vector3 f(0.0,0.0,-9.8);
        tr(it->second,itt)
        {
            f+=(*itt);
        }
        force.insert( make_pair<int, vector3 >(it->first, f) );
    }
}
void solve(domain &bodies,map<int,vector3> &force)
{
    /*
     * Solve the forces and the velocities for the local and foreign bodies
     */
    vector3 gnpn;
    map<int,vv3 > table;
    vector<pair<int,int > > contacts;



    find_contact(bodies,contacts);
    tr(contacts,it)
    {
       gnpn = pair_solver(bodies[it->first],bodies[it->second],gn);
       if(body1 in my domain)
       table[bodies[it->first]].push_back(gnpn);
       if(body2 in my domain)
       table[bodies[it->second]].push_back(-1*gnpn);

    }
    // 
    total_force(table,force);
}
#endif
